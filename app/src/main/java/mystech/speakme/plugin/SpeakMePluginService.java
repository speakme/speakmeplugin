package mystech.speakme.plugin;

import android.app.Service;
import android.content.Intent;
import android.os.HandlerThread;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import mystech.speakme.plugin.ISpeakMePlugin;
import mystech.speakme.plugin.actions.ActionBase;
import mystech.speakme.plugin.actions.ActionHandlerInterfaces;
import mystech.speakme.plugin.actions.BlockingAction;
import mystech.speakme.plugin.actions.QuerySpeechAction;
import mystech.speakme.plugin.actions.SpeechAction;
import mystech.speakme.plugin.utils.SpeakMeListener;
import mystech.speakme.plugin.utils.SpeakMeReader;
import mystech.speakme.plugin.utils.UtilInterfaces;

/**
 * SpeakMePlugin will be the parent class of ALL plugins, and it will perform critical
 * binding tasks with the other services that would otherwise need to be handled by the
 * plugin developer. In addition, this should start/stop SpeakMe passive listening service
 * while it's running.
 *
 * Finally, this should provide helper methods for everything from calling the helper services
 * and spawn the main worker method.
 */
public abstract class SpeakMePluginService extends Service implements ISpeakMePlugin,
        UtilInterfaces.IHandleOnTTSInitialized, UtilInterfaces.IHandleOnSTTInitialized {

    private SpeakMeReader ttsProvider;
    private SpeakMeListener sttProvider;

    private Intent callerIntent;

    private boolean readerInitialized = false;
    private boolean listenerInitialized = false;

    // Used for producer/consumer loop
    private BlockingDeque<ActionBase> actions;
    private int startId;

    private class ActionConsumerRunnable implements Runnable {

        @Override
        public void run() {
            while (true) {
                // Wait for a new action
                try {
                    ActionBase action = actions.take();
                    Log.d("CONSUMER THREAD", "Consuming (" + action.toString() + ")");
                    action.lockAction();
                    action.onActionStarted();
                    action.executeAction();
                    action.onActionComplete();
                    action.unlockAction();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Deals with the startService(Intent) call and delegates to our abstract method,
     * performAction() in order to implement that specific plugin. This also uses the
     * intent to start and stop the passive speech recognition based on the extras that are
     * passed into it.
     *
     * @param intent
     * @param flags
     * @param startId
     * @return START_NOT_STICKY
     */
	public int onStartCommand(final Intent intent, final int flags, final int startId) {
        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();
        this.callerIntent = intent;
        this.startId = startId;

        actions = new LinkedBlockingDeque<ActionBase>();

        Log.d("SpeakMePluginActivity", "Starting business as usual. Should stop SpeakMe again.");
        Intent stopIntent = new Intent();
        stopIntent.setAction("event-stop-broadcast");
        sendBroadcast(stopIntent);

        sttProvider = new SpeakMeListener(this, this);

        if (ttsProvider == null || !ttsProvider.isInitialized()) {
            ttsProvider = new SpeakMeReader(this, this);
        }

        // If this dies in memory, then destroy it and its resources
        return START_STICKY;
    }

    @Override
    public void onTTSInitialized() {
        readerInitialized = true;
        if (listenerInitialized) {
            runSpeechApp();
        }
    }

    @Override
    public void onSSTInitialized() {
        listenerInitialized = true;
        if (readerInitialized) {
            runSpeechApp();
        }
    }

    ExecutorService thread;
    private void runSpeechApp() {
        thread = Executors.newFixedThreadPool(2);
        thread.submit(new ActionConsumerRunnable());

        onInitialized();

        String text = "";
        if (callerIntent.getExtras() != null) {
            text = callerIntent.getStringExtra("TEXT");
        }
        performAction(text);

        while(actions.size() > 0)
        {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void shutdown() {
        Log.d("SpeakMePluginService","Shutting your shiz down");
        thread.shutdown();

        ttsProvider.shutdown();
        sttProvider.shutdown();

        Log.d("SpeakMePluginActivity", "Stopping business as usual. Should start up SpeakMe again.");
        Intent stopIntent = new Intent();
        stopIntent.setAction("event-start-broadcast");
        sendBroadcast(stopIntent);

        stopSelf(startId);
    }

    /**
     * This is the main method and entry point for SpeakMe Plugins. Implement or delegate
     * plugin functionality from this method. This is executed in a background thread, so make
     * sure to use getMainLooper() to post on the main thread.
     * @param text The text sent from the core app with the text that was used to launch this
     *             plugin including the plugin name.
     */
    protected abstract void performAction(String text);
    protected abstract void onInitialized();

    public void queueAction(ActionBase action, boolean addToFront) {
        if (addToFront) {
            actions.addFirst(action);
        } else {
            actions.add(action);
        }
    }

    public void queueSpeech(String speech) {
        queueSpeech(speech, false);
    }

    public void queueSpeech(String speech, boolean immediately) {
        queueSpeech(speech, immediately, null);
    }

    public void queueSpeech(final String speech, final boolean immediately, ActionHandlerInterfaces.IHandleActionSuccess iHandleActionSuccess) {

        final BlockingAction action = new SpeechAction(ttsProvider, speech);
        action.registerOnSuccessListener(iHandleActionSuccess);

        if (immediately) {
            actions.addFirst(action);
        } else {
            actions.add(action);
        }
    }

    /**
     * Returns a String[] of possibilities generated by the Speech to Text parser. This
     * overloaded method deals with error handling internally, where the error message is
     * passed in and the desired behavior for processing that error.
     * @param prompt The string to initially present the user for a prompt.
     * @param errorPrompt The string to speak on an error happening.
     * @param repeatOnError Indicates whether to continue (true) or not (false) on error
     * @param repeatPromptOnError Indicates whether to re-speak the prompt on error.
     * @return String[] of possibilities in descending order
     */
    public void queryUser(String prompt, String errorPrompt, boolean repeatOnError,
                          boolean repeatPromptOnError, UtilInterfaces.IHandlePartialSpeechResults iHandlePartialSpeechResults,
                          UtilInterfaces.IHandleSpeechResults iHandleSpeechResults) {
        final QuerySpeechAction action =  new QuerySpeechAction(iHandlePartialSpeechResults, iHandleSpeechResults,
                sttProvider, ttsProvider, prompt, errorPrompt, repeatOnError, repeatPromptOnError);
        actions.add(action);
    }

    /**
     * Returns a String[] of possibilities returned by the Speech to Text parser. There is no
     * query, and you must handle errors yourself.
     * @return String[] of possibilities in descending order
     * @throws mystech.speakme.plugin.InvalidSpeechException
     */
    public void queryUser(UtilInterfaces.IHandlePartialSpeechResults iHandlePartialSpeechResults,
                          UtilInterfaces.IHandleSpeechResults iHandleSpeechResults) {
        queryUser(null, null, false, false, iHandlePartialSpeechResults, iHandleSpeechResults);
    }

    /**
     * Returns a String[] of possibilities returned by the Speech to Text parser. There is a given
     * query, and you must handle errors yourself.
     * @param prompt The prompt for the question.
     * @return String[] of possibilities in descending order
     * @throws mystech.speakme.plugin.InvalidSpeechException
     */
    public void queryUser(String prompt, UtilInterfaces.IHandlePartialSpeechResults iHandlePartialSpeechResults,
                          UtilInterfaces.IHandleSpeechResults iHandleSpeechResults) {
        queryUser(prompt, null, false, false, iHandlePartialSpeechResults, iHandleSpeechResults);
    }

    public void queryUser(String query, UtilInterfaces.IHandlePartialSpeechResults iHandlePartialSpeechResults,
                          UtilInterfaces.IHandleSpeechResults iHandleSpeechResults,
                          ActionHandlerInterfaces.IHandleActionError iHandleActionError) {
        final QuerySpeechAction action =  new QuerySpeechAction(iHandlePartialSpeechResults, iHandleSpeechResults,
                sttProvider, ttsProvider, query, null, false, false);
        action.registerOnErrorListener(iHandleActionError);
        actions.add(action);
    }
}
