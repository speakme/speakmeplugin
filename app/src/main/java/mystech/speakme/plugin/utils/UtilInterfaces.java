package mystech.speakme.plugin.utils;

/**
 * Created by stephen on 7/20/14.
 */
public class UtilInterfaces{

    /**
     * Indicates that the class needs to call custom code when the TTS is finally
     * initialized.
     */
    public interface IHandleOnTTSInitialized
    {

        /**
         * Called when the TTS engine has been successfully set up.
         */
        void onTTSInitialized();
    }

    /**
     * Indicates that the class needs to call custom code when the STT is finally
     * initialized.
     */
    public interface IHandleOnSTTInitialized
    {
        /**
         * Called when the TTS engine has been successfully set up.
         */
        void onSSTInitialized();
    }

    /**
     * This is called when speech results are available.
     */
    public interface IHandleSpeechResults
    {
        /**
         * Called when speech results are available
         * @param results Array of possible string results.
         */
        void resultsAvailable(String[] results);
    }

    /**
     * Used when partial results are desired
     */
    public interface IHandlePartialSpeechResults
    {
        /**
         * Called when speech results are partially available.
         * @param results Array of possible string results.
         */
        void partialResultsAvailable(String[] results);
    }

}
