package mystech.speakme.plugin.utils;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;

/**
 * Controls the SpeakMePlugin's text-to-speech reader, and deals with the utterances
 * necessary to create a blocking situation.
 */
public class SpeakMeReader {
    private TextToSpeech tts;
    private Context parent;
    private UtilInterfaces.IHandleOnTTSInitialized initListener;

    private boolean speechReady;
    private boolean speaking;
    private String utteranceID = "utterance_id_speech";

    /**
     * Creates a new android.speech.tts.TextToSpeech object and sets it up. This involves making a
     * new UtteranceListener, that will set a member boolean to true/false based on whether the
     * tts is currently speaking or is finished.
     * @param parent Refers to the owner of this class, so it's attrivutes can be used.
     */
    public SpeakMeReader(final Context parent, final UtilInterfaces.IHandleOnTTSInitialized initListener) {
        this.parent = parent;
        this.initListener = initListener;
        speechReady = false;
        tts = new TextToSpeech(parent, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {

                    if (tts.isLanguageAvailable(Locale.US) == TextToSpeech.LANG_AVAILABLE)
                        tts.setLanguage(Locale.US);

                    // Set to a language locale after checking availability
                    Log.i("READER", "available=" + tts.isLanguageAvailable(Locale.US));
                    tts.setLanguage(Locale.US);

                    tts.setPitch(1.0F);
                    tts.setSpeechRate(1.0F);
                    speechReady = true;

                    if (initListener != null) {
                        initListener.onTTSInitialized();
                    }

                    Log.d("TTS", "Initilization Success!");
                }
                else {
                    Log.e("TTS", "Initilization Failed!");
                }
            }
        });

        tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
                speaking = true;
            }

            @Override
            public void onDone(String utteranceId) {
                Log.d("TTS", "On Done");
                speaking = false;
            }

            @Override
            public void onError(String utteranceId) {
                Log.d("TTS", "On Done");
                speaking = false;
            }
        });
    }

    /**
     * Returns true if this service is not currently speaking, and it returns false if
     * the utterance listener has been started but not finished.
     * @return true if not currently speaking, false otherwise.
     */
    public boolean isDoneSpeaking() {
        return !speaking && !tts.isSpeaking() ;
    }

    /**
     * Query whether the speech engine is ready or not.
     * @return true if the TTS engine is set up.
     */
    public boolean isInitialized() {
        return speechReady ;
    }

    /**
     * Ensure that the tts engine is shut down.
     */
    public void shutdown() {
        tts.shutdown();
        speaking = false;
    }

    /**
     * Detects whether the input is valid, and if so, will invoke the text-to-speech
     * reader. Its helper method will also explicitly ensure that the system volume is unmuted,
     * as the main SpeakMe app may mute it temporarily.
     * @param text The text to pass to the tts reader
     */
    public void invokeTTS(String text, boolean immediately) {
        Log.d("SpeakMereader", "Queueing text: " + text);
        if (text != null && text.trim().length() != 0) {
            speaking = true;
            textToSpeech(text, immediately);
        }
    }

    /**
     * Adds the given text to the speech queue after ensuring that the system volume is unmuted.
     * @param text The text to pass to the tts reader
     */
    private void textToSpeech(String text, boolean immediately) {

        HashMap<String, String> voiceMap = new HashMap();
        voiceMap.put(TextToSpeech.Engine.KEY_FEATURE_NETWORK_SYNTHESIS,"true");
        voiceMap.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,"messageID");

        if (immediately) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, voiceMap);
        }
        else {
            tts.speak(text, TextToSpeech.QUEUE_ADD, voiceMap);
        }
        Log.d("TTS", "finished Queueing");
    }

}
