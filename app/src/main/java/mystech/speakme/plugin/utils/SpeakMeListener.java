package mystech.speakme.plugin.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import java.util.ArrayList;

/**
 * Retrieves speech from the user and provides different methods for handling errors that arise
 * from it. Note that all Speech-To-Text operations must occur on the Main Application thread, and
 * therefore, we must discover it.
 */
public class SpeakMeListener {

    private int silenceTimout;
    private SynchronizedCallback callback = new SynchronizedCallback();
    private Handler mainThread;

    private SpeechRecognizer speechService;

    /**
     * Sets the Service's parent and discovers the application main thread, which is necessary for
     * performing Speech-To-Text operations.
     *
     */
    public SpeakMeListener(Context parent, final UtilInterfaces.IHandleOnSTTInitialized initListener) {
        this.mainThread = new Handler(parent.getApplicationContext().getMainLooper());

        speechService = SpeechRecognizer.createSpeechRecognizer(parent);
        speechService.setRecognitionListener(new SpeakMeRecognizer());

        initListener.onSSTInitialized();
    }

    public void queryUser() {

        callback = new SynchronizedCallback();

        mainThread.postAtFrontOfQueue(new Runnable() {
            @Override
            public void run() {
                getInput();
            }
        });
    }

    /**
     * Helper method for queryUser(), this method is invoked in the main thread and is responsible
     * for starting the speech service that is used to query the user.
     */
    private void getInput() {
        // Set up the intent for 5 results in Dictation mode and no extra prompt.
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
//        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, silenceTimout);
//        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS, silenceTimout);
//        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, silenceTimout);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, this.getClass().getPackage() + "");
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, false);
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
        speechService.startListening(intent);
    }

    /**
     * Ensure that the parent and mainThread references are destroyed, so that the garbage collector
     * can remove them if necessary. This prevents memory leaks.
     */
    public void shutdown() {
        mainThread.postAtFrontOfQueue(new Runnable() {
            @Override
            public void run() {
                // Now, we also have to cancel and destroy the service on the main thread. Neither
                // cancel nor destroy works by itself, so both must be called in order to not have a
                // memory leak upon leaving the plugin.

                speechService.cancel();
                speechService.destroy();
            }
        });
    }

    public SynchronizedCallback getCallback() {
        return callback;
    }

    public class SynchronizedCallback {
        public volatile boolean error;
        public volatile String[] results;
        public volatile String[] partialResults;
        public volatile boolean finished;
    }

    /**
     * Sets the callback variables appropriately onError() and onResult(). This class is used in
     * on the main thread to detect and respond to speech.
     */
    private class SpeakMeRecognizer implements RecognitionListener {

        @Override
        public void onReadyForSpeech(Bundle bundle) { Log.d("SPEAKME LISTENER", "In onReadyForSpeech"); }

        @Override
        public void onBeginningOfSpeech() { Log.d("SPEAKME LISTENER", "In onBeginningOfSpeech"); }

        @Override
        public void onRmsChanged(float v) {}

        @Override
        public void onBufferReceived(byte[] bytes) { }

        @Override
        public void onEndOfSpeech() { Log.d("SPEAKME LISTENER", "In onEndOfSpeech"); }

        @Override
        public void onError(int i) {
            Log.d("SPEAKME LISTENER", "In onOnError");

            callback.finished = true;
            callback.error = true;
        }

        @Override
        public void onResults(Bundle bundle) {
            Log.d("SPEAKME LISTENER", "In onResults");

            // Retrieves the list of possible results.
            ArrayList<String> results = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

            callback.finished = true;
            callback.results = results.toArray(new String[]{});
        }

        @Override
        public void onPartialResults(Bundle bundle) {
            Log.d("SPEAKME LISTENER", "In onPartialResults");

            // Retrieves the list of possible results.
            ArrayList<String> results = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

            callback.finished = false;
            callback.partialResults = results.toArray(new String[]{});
        }

        @Override
        public void onEvent(int i, Bundle bundle) {}
    }
}
