package mystech.speakme.plugin.actions;

import android.util.Log;

import mystech.speakme.plugin.utils.SpeakMeListener;
import mystech.speakme.plugin.utils.SpeakMeReader;
import mystech.speakme.plugin.utils.UtilInterfaces;

/**
 * Created by stephen on 7/20/14.
 */
public class QuerySpeechAction extends BlockingAction {

    private static final String TAG = "QuerySpeechAction";

    private UtilInterfaces.IHandlePartialSpeechResults iHandlePartialSpeechResults;
    private UtilInterfaces.IHandleSpeechResults iHandleSpeechResults;
    private SpeakMeListener sstService;
    private SpeakMeReader ttsService;
    private String prompt;
    private String errorPrompt;
    private boolean repeatOnError;
    private boolean repeatPromptOnError;

    public QuerySpeechAction(UtilInterfaces.IHandlePartialSpeechResults partialSpeechResults,
                             UtilInterfaces.IHandleSpeechResults speechResults,
                             SpeakMeListener listener, SpeakMeReader reader, String prompt, String errorPrompt,
                             boolean repeatOnError, boolean repeatPromptOnError) {
        this.iHandlePartialSpeechResults = partialSpeechResults;
        this.iHandleSpeechResults = speechResults;
        this.sstService = listener;
        this.ttsService = reader;
        this.prompt = prompt;
        this.errorPrompt = errorPrompt;
        this.repeatOnError = repeatOnError;
        this.repeatPromptOnError = repeatPromptOnError;
    }

    @Override
    public void executeAction() {
        String[] returnArray = null;
        boolean repeated = false;
        do {

            // If we are on the first iteration OR have repeatPrompt turned on, respeak
            if (repeated && !repeatPromptOnError) {
                sstService.queryUser();
                waitForSpeechFinish();

            } else {
                speak(prompt);
                sstService.queryUser();
                waitForSpeechFinish();
            }
            if (sstService.getCallback().error) {
                speak(errorPrompt);
                repeated = true;
            }
        } while (repeatOnError && sstService.getCallback().results == null);

        iHandleSpeechResults.resultsAvailable(sstService.getCallback().results);
    }

    private void speak(String speech) {
        ttsService.invokeTTS(speech, true);
        while (!ttsService.isDoneSpeaking()) {
            try {
                synchronized (this) {
                    wait(200);
                }
            } catch (InterruptedException e) {
                onActionError(e);
            }
        }
    }

    private void waitForSpeechFinish() {
        while (!sstService.getCallback().finished) {
            if (sstService.getCallback().partialResults != null) {
                if (iHandlePartialSpeechResults != null) {
                    iHandlePartialSpeechResults.partialResultsAvailable(sstService.getCallback().partialResults);
                }
            }
            try {
                synchronized (this) {
                    wait(200);
                }
            } catch (InterruptedException e) {
                onActionError(e);
            }
        }
    }

    @Override
    public void onActionStarted() {
        Log.d(TAG, _id + ": onActionStart()");
        if (actionStartedHandler != null) {
            actionStartedHandler.onActionStarted(this);
        }
    }

    @Override
    public void onActionComplete() {
        Log.d(TAG, _id + ": onActionComplete()");
        if (actionCompleteHandler != null) {
            actionCompleteHandler.onActionComplete(this);
        }
        SemaphoreManager.LOCK.release();
    }

    @Override
    public void onActionError(Exception e) {
        Log.d(TAG, _id + ": onActionError()");
        if (actionErrorHandler != null) {
            actionErrorHandler.onActionError(this,e);
        }
    }

    @Override
    public void onActionSuccess() {
        Log.d(TAG, _id + ": onActionSuccess()");
        if (actionSuccessHandler != null)
        {
            actionSuccessHandler.onActionSuccess(this);
        }
    }
}
