package mystech.speakme.plugin.actions;

/**
 * Created by stephen on 7/17/14.
 */
public abstract class BlockingAction extends ActionBase {

    public void lockAction() throws InterruptedException {
        SemaphoreManager.LOCK.acquire();
    }

    public void unlockAction() {
        SemaphoreManager.LOCK.release();
    }
}
