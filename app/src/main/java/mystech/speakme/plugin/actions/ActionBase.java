package mystech.speakme.plugin.actions;


/**
 * Created by stephen on 7/15/14.
 */
public abstract class ActionBase {

    private static int internalActionNum;
    static {
        internalActionNum = 0;
    }

    protected int _id;
    public ActionBase() {
        _id = ++internalActionNum;
    }

    public int getID() {
        return _id;
    }

    public abstract void executeAction();
    public abstract void lockAction() throws InterruptedException;
    public abstract void unlockAction();
    public abstract void onActionStarted();
    public abstract void onActionComplete();
    public abstract void onActionSuccess();
    public abstract void onActionError(Exception e);

    protected ActionHandlerInterfaces.IHandleActionStarted actionStartedHandler;
    public void registerOnStartListener(ActionHandlerInterfaces.IHandleActionStarted actionStartedHandler)
    {
        this.actionStartedHandler = actionStartedHandler;
    }

    protected ActionHandlerInterfaces.IHandleActionComplete actionCompleteHandler;
    public void registerOnCompleteListener(ActionHandlerInterfaces.IHandleActionComplete actionCompleteHandler)
    {
        this.actionCompleteHandler = actionCompleteHandler;
    }

    protected ActionHandlerInterfaces.IHandleActionError actionErrorHandler;
    public void registerOnErrorListener(ActionHandlerInterfaces.IHandleActionError actionErrorHandler)
    {
        this.actionErrorHandler = actionErrorHandler;
    }

    protected ActionHandlerInterfaces.IHandleActionSuccess actionSuccessHandler;
    public void registerOnSuccessListener(ActionHandlerInterfaces.IHandleActionSuccess actionSuccessHandler)
    {
        this.actionSuccessHandler = actionSuccessHandler;
    }

}
