package mystech.speakme.plugin.actions;

/**
 * Created by stephen on 7/20/14.
 */
public class ActionHandlerInterfaces {
    public interface IHandleActionStarted {
        public void onActionStarted(ActionBase action);
    }

    public interface IHandleActionComplete {
        public void onActionComplete(ActionBase action);
    }

    public interface IHandleActionError {
        public void onActionError(ActionBase action, Exception e);
    }

    public interface IHandleActionSuccess {
        public void onActionSuccess(ActionBase action);
    }
}
