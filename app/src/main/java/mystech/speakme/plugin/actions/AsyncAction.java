package mystech.speakme.plugin.actions;

/**
 * Created by stephen on 7/17/14.
 */
public abstract class AsyncAction extends ActionBase {

    public void lockAction() throws InterruptedException {
        // Do not lock! We're Async
    }

    public void unlockAction() {
        // Nothing to unlock.
    }
}