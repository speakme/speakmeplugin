package mystech.speakme.plugin.actions;

import android.util.Log;

import mystech.speakme.plugin.utils.SpeakMeReader;

/**
 * Created by stephen on 7/15/14.
 */
public class SpeechAction extends BlockingAction {

    private final String TAG = "SPEECH_ACTION";

    private SpeakMeReader ttsService;
    private String speech;

    public SpeechAction(SpeakMeReader ttsService, String speech) {
        this.ttsService = ttsService;
        this.speech = speech;
    }

    @Override
    public void executeAction() {
        Log.d(TAG, _id + ": executeAction()");
        ttsService.invokeTTS(this.speech, true);
        while (!ttsService.isDoneSpeaking()) {
            try {
                synchronized (this) {
                    wait(200);
                }
            } catch (InterruptedException e) {
                onActionError(e);
            }
        }
        onActionSuccess();
    }

    @Override
    public void onActionStarted() {
        Log.d(TAG, _id + ": onActionStarted()");
        if (actionStartedHandler != null) {
            actionStartedHandler.onActionStarted(this);
        }
    }

    @Override
    public void onActionComplete() {
        Log.d(TAG, _id + ": onActionComplete()");
        if (actionCompleteHandler != null) {
            actionCompleteHandler.onActionComplete(this);
        }
    }

    @Override
    public void onActionSuccess() {
        Log.d(TAG, _id + ": onActionSuccess()");
        if (actionSuccessHandler != null) {
            actionSuccessHandler.onActionSuccess(this);
        }
    }

    @Override
    public void onActionError(Exception e) {
        Log.d(TAG, _id + ": onActionError()");
        if (actionErrorHandler != null) {
            actionErrorHandler.onActionError(this, e);
        }
    }
}
