package mystech.speakme.plugin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import mystech.speakme.plugin.actions.ActionBase;
import mystech.speakme.plugin.actions.ActionHandlerInterfaces;
import mystech.speakme.plugin.actions.BlockingAction;
import mystech.speakme.plugin.actions.QuerySpeechAction;
import mystech.speakme.plugin.actions.SpeechAction;
import mystech.speakme.plugin.utils.SpeakMeListener;
import mystech.speakme.plugin.utils.SpeakMeReader;
import mystech.speakme.plugin.utils.UtilInterfaces;

/**
 * Extend this if you want to implement activities in SpeakMe. Typically used for things like
 * settings. This class ensures that the main SpeakMe app starts and stops on time.
 */
public abstract class SpeakMePluginActivity extends Activity implements ISpeakMePlugin,
        UtilInterfaces.IHandleOnTTSInitialized, UtilInterfaces.IHandleOnSTTInitialized {

    private static final int CHECK_DATA = 0;

    private SpeakMeReader ttsProvider;
    private SpeakMeListener sttProvider;

    private boolean readerInitialized = false;
    private boolean listenerInitialized = false;

    private ExecutorService threadServices;

    // Used for producer/consumer loop
    private BlockingDeque<ActionBase> actions;

    private class ActionConsumerRunnable implements Runnable {

        @Override
        public void run() {
            while (true) {
                // Wait for a new action
                try {
                    ActionBase action = actions.take();
                    action.lockAction();
                    action.onActionStarted();
                    action.executeAction();
                    action.onActionComplete();
                    action.unlockAction();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Delegates to the super.onCreate(Bundle) method, but also sends out an event broadcast that
     * is intended to stop the SpeakMe app from listening.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("SpeakMePluginActivity", "In OnCreate()");

        actions = new LinkedBlockingDeque<ActionBase>();
    }

    /**
     * Delegates to the super.onResume() method, but also sends out an event broadcast that
     * is intended to stop the SpeakMe app from listening.
     */
    @Override
    protected final void onResume() {
        super.onResume();

        Log.d("SpeakMePluginActivity", "Starting business as usual. Should stop SpeakMe again.");
        Intent stopIntent = new Intent();
        stopIntent.setAction("event-stop-broadcast");
        sendBroadcast(stopIntent);

        if (sttProvider == null) {
            sttProvider = new SpeakMeListener(this, this);
        }

        // Connect to the TTS and Speech to Text providers.
        if (ttsProvider == null || ttsProvider.isInitialized()) {
            Intent checkIntent = new Intent();
            checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
            startActivityForResult(checkIntent, CHECK_DATA);
        }
    }

    @Override
    public void onTTSInitialized() {
        readerInitialized = true;
        if (listenerInitialized) {
            runSpeechApp();
        }
    }

    @Override
    public void onSSTInitialized() {
        listenerInitialized = true;
        if (readerInitialized) {
            runSpeechApp();
        }
    }

    private void runSpeechApp()
    {
        threadServices = Executors.newFixedThreadPool(1);
        threadServices.submit(new ActionConsumerRunnable());

        onInitialized();

        String text = "";
        if (getIntent() != null) {
            text = getIntent().getStringExtra("TEXT");
        }
        performAction(text);
    }

    protected abstract void onInitialized();
    protected abstract void performAction(String text);

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CHECK_DATA) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {

                // Success, so create the TTS instance.  But can't use it to speak until
                // the onTTSIntialized(status) callback defined below runs, indicating initialization.
                ttsProvider = new SpeakMeReader(this, this);

            } else {
                // missing data, so install it on the device
                Log.i("SpeakMePluginActivity", "Missing Data; Install it");
                Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }

    /**
     * Delegates to the super.onStop() method, but also sends out an event broadcast that
     * is intended to start the SpeakMe app listener again.
     */
    @Override
    protected void onStop() {
        super.onStop();

        ttsProvider.shutdown();
        sttProvider.shutdown();
        threadServices.shutdown();

        Log.d("SpeakMePluginActivity", "Stopping business as usual. Should start up SpeakMe again.");
        Intent stopIntent = new Intent();
        stopIntent.setAction("event-start-broadcast");
        sendBroadcast(stopIntent);
    }

    public void queueAction(ActionBase action, boolean addToFront) {
        if (addToFront) {
            actions.addFirst(action);
        } else {
            actions.add(action);
        }
    }

    public void queueSpeech(String speech) {
        queueSpeech(speech, false);
    }

    public void queueSpeech(String speech, boolean immediately) {
        queueSpeech(speech, immediately, null);
    }

    public void queueSpeech(String speech, boolean immediately, ActionHandlerInterfaces.IHandleActionSuccess iHandleActionSuccess) {
        queueSpeech(speech, immediately, iHandleActionSuccess, null);
    }

    public void queueSpeech(final String speech, final boolean immediately, ActionHandlerInterfaces.IHandleActionSuccess iHandleActionSuccess,
                            ActionHandlerInterfaces.IHandleActionError iHandleActionError) {

        final BlockingAction action = new SpeechAction(ttsProvider, speech);
        action.registerOnSuccessListener(iHandleActionSuccess);
        action.registerOnErrorListener(iHandleActionError);

        if (immediately) {
            actions.addFirst(action);
        } else {
            actions.add(action);
        }
    }

    /**
     * Returns a String[] of possibilities generated by the Speech to Text parser. This
     * overloaded method deals with error handling internally, where the error message is
     * passed in and the desired behavior for processing that error.
     * @param prompt The string to initially present the user for a prompt.
     * @param errorPrompt The string to speak on an error happening.
     * @param repeatOnError Indicates whether to continue (true) or not (false) on error
     * @param repeatPromptOnError Indicates whether to re-speak the prompt on error.
     * @return String[] of possibilities in descending order
     */
    public void queryUser(String prompt, String errorPrompt, boolean repeatOnError,
                          boolean repeatPromptOnError, UtilInterfaces.IHandlePartialSpeechResults iHandlePartialSpeechResults,
                          UtilInterfaces.IHandleSpeechResults iHandleSpeechResults) {
        final QuerySpeechAction action =  new QuerySpeechAction(iHandlePartialSpeechResults, iHandleSpeechResults,
                sttProvider, ttsProvider, prompt, errorPrompt, repeatOnError, repeatPromptOnError);
        actions.add(action);
    }

    /**
     * Returns a String[] of possibilities returned by the Speech to Text parser. There is no
     * query, and you must handle errors yourself.
     * @return String[] of possibilities in descending order
     * @throws InvalidSpeechException
     */
    public void queryUser(UtilInterfaces.IHandlePartialSpeechResults iHandlePartialSpeechResults,
                              UtilInterfaces.IHandleSpeechResults iHandleSpeechResults) {
        queryUser(null, null, false, false, iHandlePartialSpeechResults, iHandleSpeechResults);
    }

    /**
     * Returns a String[] of possibilities returned by the Speech to Text parser. There is a given
     * query, and you must handle errors yourself.
     * @param prompt The prompt for the question.
     * @return String[] of possibilities in descending order
     * @throws InvalidSpeechException
     */
    public void queryUser(String prompt, UtilInterfaces.IHandlePartialSpeechResults iHandlePartialSpeechResults,
                              UtilInterfaces.IHandleSpeechResults iHandleSpeechResults) {
        queryUser(prompt, null, false, false, iHandlePartialSpeechResults, iHandleSpeechResults);
    }

    public void queryUser(String query, UtilInterfaces.IHandlePartialSpeechResults iHandlePartialSpeechResults,
                          UtilInterfaces.IHandleSpeechResults iHandleSpeechResults,
                          ActionHandlerInterfaces.IHandleActionError iHandleActionError) {
        final QuerySpeechAction action =  new QuerySpeechAction(iHandlePartialSpeechResults, iHandleSpeechResults,
                sttProvider, ttsProvider, query, null, false, false);
        action.registerOnErrorListener(iHandleActionError);
        actions.add(action);
    }
}
